# demo2

## Summary
demo2 shows 2 filled rectangles draw on screen rotating around their point and around another origin

## License
[SPDX-License-Identifier: GPL-3.0-or-later](https://spdx.org/licenses/GPL-3.0-or-later.html)

Copyright (c) 2021 parlortricks