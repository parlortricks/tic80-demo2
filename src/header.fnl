;; title:   Flood filled rotating vector graphics
;; author:  parlortricks
;; desc:    demo2 shows 2 filled rectangles draw on screen rotating around their point and around another origin
;; website: http://tic80.com/dev?id=6589
;; script:  fennel
;; SPDX-License-Identifier: GPL-3.0-or-later
;; Copyright (c) 2021 parlortricks

(local screen-width 240)
(local screen-height 136)
(local sin math.sin)
(local cos math.cos)
(local atan2 math.atan2)
(var r 0)

